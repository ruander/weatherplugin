<?php
/**
 * Plugin Name: Időjárás jelző
 * Plugin URI:  https://developer.wordpress.org/plugins/the-basics/
 * Description: Az admin felületen a felhasználó sávban mutatja egy beállítható város időjárás adatait
 * Version:     1.0.0
 * Author:      Horváth György
 * Author URI:  https://developer.wordpress.org/
 * Text Domain: myweather
 * Domain Path: /languages
 * License:     GPL2
 */

function toolbar_weather_addon($wp_admin_bar)
{
    $weatherData = json_decode(file_get_contents('http://api.openweathermap.org/data/2.5/weather?q=Budapest,HU&lang=hu&units=metric&appid=5d9466cfdac002c3c4b77eb077e35de4'), true);
    //echo $weatherData['weather'][0]['main'];
    $iconSrc = 'https://openweathermap.org/img/wn/'.$weatherData['weather'][0]['icon'].'.png';
    //echo '<pre>' . var_export($weatherData, true) . '</pre>';
    $args = array(
        'id' => 'my_page',
        'parent' => 'top-secondary',
        'title' => 'időjárás Budapesten: '
            .$weatherData['main']['temp'] //hőmérséklet
            .' &#8451; <img src="'.$iconSrc.'" title="'.$weatherData['weather'][0]['description'].'"> '//icon
            ,
        'meta' => array(
            //'html' => '<div>test</div>',
            'class' => 'my-toolbar-weather'
        )
    );
    $wp_admin_bar->add_node($args);
}

add_action('admin_bar_menu', 'toolbar_weather_addon', 999);

//css file betöltése az admin felületre:
function load_custom_wp_admin_style($hook) {
    wp_enqueue_style( 'custom_wp_admin_css', plugins_url('css/style.css', __FILE__) );
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

// Add Shortcode
function hgy_weather_shortcode( $atts ) {

    // Attributes
    $atts = shortcode_atts(
        array(
            'city' => '',
        ),
        $atts,
        'hgy-weather'
    );

    // Return only if has city attribute
    if ( isset( $atts['city'] ) ) {
        $weatherData = json_decode(file_get_contents('http://api.openweathermap.org/data/2.5/weather?q='.$atts['city'].'&lang=hu&units=metric&appid=5d9466cfdac002c3c4b77eb077e35de4'), true);
        $iconSrc = 'https://openweathermap.org/img/wn/'.$weatherData['weather'][0]['icon'].'.png';
        $content= 'időjárás Budapesten: '
            .$weatherData['main']['temp'] //hőmérséklet
            .' &#8451; <img src="'.$iconSrc.'" title="'.$weatherData['weather'][0]['description'].'"> ';
        return '<div class="hgy-weather-widget">'.$content.'</div>';
    }
}
add_shortcode( 'hgy-weather', 'hgy_weather_shortcode' );